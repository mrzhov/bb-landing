## Run in dev mode

1. `yarn install`
1. `yarn run dev`

## Build project using Docker

1. Build your container: `docker build -t nextjs-docker .`
1. Run your container: `docker run -p 3000:3000 nextjs-docker`