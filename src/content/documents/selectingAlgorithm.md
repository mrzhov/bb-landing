# Проектное решение: универсальный формат хранения и обмена машиночитаемыми нормативными правовыми актами

## Общие принципы и подходы в реализации. Выбор технологий





### Авторы проекта:

Лахтин Станислав

Королева Мария


### Эксперты:

Аникин Максим

Будников Константин

Стрельцов Алексей

---

В рамках проекта используется “Система классификации машиночитаемости формата нормативного правового акта".

Введение подобной и точно определенной шкалы в терминах и понятиях позволяет снять неопределенности и двусмысленности, определить функциональные ограничения для существующих и разрабатываемых систем, связанные с использованием форматов установленного уровня машиночитаемости форматов НПА.

Описываемый в рамках текущего документа формат относится к *уровню машиночитаемости 3b* в рамках упомянутой системы классификации.

## Общие принципы и подходы в реализации

Раздел описывает принципы, которые определяют оптимальный выбор способа кодирования нормативных правовых актов в виде информационных объектов.

Здесь будет рассмотрена масса исчерпывающих примеров спецификаций, определяющих различные подходы к кодированию информации предметной области и влияющие на выбор ограничения. Раздел постепенно вводит понимание ограничений на кодирование, позволяя читателю понять, почему выбор способа кодирования, предлагаемый в рамках спецификации, именно такой.

Следует сделать общее замечание о том, что под форматом кодирования информации мы понимаем подход, при котором над данными в цифровой форме, находящимися в памяти компьютера, осуществляется операция сериализации/десериализации для передачи между устройствами или сохранении данных с использованием постоянного запоминающего устройства (не важно какой технологии) для последующего восстановления в память компьютера.

При этом следует разделять понятия сериализации экземпляра данных и хранения/передачи всей совокупности сведений, которые, как правило, представляют собой сложный, составной информационный объект, включающий сведения сразу о нескольких сериализованных объектах.

### О файловых ресурсах

Поскольку нормативный правовой акт представляет собой совокупность сведений, то он не может быть сохранен эффективно и рационально в рамках одного сериализованного объекта.

В связи с тем, что общепринятым подходом в работе с данными долгосрочного хранения является работа в рамках соглашений об использовании файловых ресурсов, а сама концепция файла неразрывно связана с понятием файловых структур или файловой системы, то нормативный правовой документ удобнее размещать в рамках отдельной файловой структуры, состоящей из нескольких файловых ресурсов. А спецификация, описывающая нормативный правовой акт в виде одного файла, обязана опираться на такой формат “внешнего контейнера”, который бы объединял несколько файлов (структуру) формализованным образом в один. По сути НПА является не файлом, а файловой структурой.

Из присутствующих на рынке в настоящий момент контейнеров упомянутого типа наилучшим образом соответствует требованиям формат ZIP, первоначально созданный в рамках компании PKWARE и развитый в JAR (EAR, RAR (Java), WAR), Office Open XML (Microsoft), Open Packaging Conventions, OpenDocument (ODF), XPI (расширения Mozilla) в рамках открытой спецификации, все являющиеся архивами ZIP файлов и решающие схожие с нашей предметной областью задачи. ZIP может считаться общеиспользуемым и широко распространенным форматом архивации данных, поддерживаемым на уровне распространенных файловых систем без необходимости установки дополнительного ПО, а кроме того, широко поддерживаемым отраслевым стандартом с большим количеством программных библиотек, снижающими риск зависимости от санкционных ограничений.

К достоинствам ZIP следует отнести не только широкую распространенность и качественный алгоритм сжатия, но и возможность установки пароля на формируемый документ (структуру, объединяющую несколько связанных информационных объектов). Это позволяет на уровне контейнера определить возможность сокрытия информации в рамках НПА, в случае применения перспективного формата для построения систем с ограничением доступа к информации.

Отсюда и далее под ДОКУМЕНТОМ мы будем понимать такой контейнеризованный информационный объект, который исчерпывающе описывает нормативный правовой акт.

 	ДОКУМЕНТ (РЕДАКЦИЯ) НПА представляет собой архивную версию структуры файлов (наподобие JAR и RAR(Java) спецификаций), определяющую внутреннюю иерархию файловых ресурсов, помещенных в этот общий формат.

Соглашение на способ кодирования ДОКУМЕНТА (редакции) НПА  определяет его общую совместимость с подходами, реализуемыми в рамках отраслевых спецификаций ISO/IEC JTC 1/SC 34. Например, ICS > 35 > 35.240 > 35.240.30 ISO/IEC 21320-1:2015 — Information technology / Document Container File (контейнер файлов).

Чтобы устранить путаницу, будем использовать для обозначения формата ДОКУМЕНТА (редакции) НПА акроним LAW — Legal Act for Work, а когда говорим о формате файла ДОКУМЕНТА — выражение “в формате *.law”.

## Выбор технологий

Использование той или иной технологии для реализации предполагает преимущественное использование открытых отраслевых стандартов и спецификаций. Не допускается использование ограничивающих технологий, которые практически могут заблокировать развитие спецификации или сузить ее применение.

Всегда, когда это не определено явно, предполагается, что не существует никаких ограничений на использование этой спецификации в интересах государственного управления Российской Федерации, открытого использования технологии в коммерческих целях, в том числе на основании государственных данных сторонними коммерческими организациями в интересах повышения качества жизни граждан Российской Федерации.

Авторами спецификации НЕ ДОПУСКАЕТСЯ применение технологий, определяющих порядок лицензионных отчислений на использование технологии целиком или ее частей.
