import React from 'react';

import content from '@/content/home.json';

const HomeSection = (): React.ReactElement => {
	return (
		<section id='home' className='d-flex align-items-center justify-content-center'>
			<div className='container' data-aos='fade-up'>
				<div className='row justify-content-center' data-aos='fade-up' data-aos-delay='150'>
					<div className='col-xl-6 col-lg-8'>
						<h1 id='home-title'>{content.title}</h1>
						<h2 id='home-subtitle'>{content.subtitle}</h2>
					</div>
				</div>
				<div
					id='home-boxes-container'
					className='row gy-4 mt-5 justify-content-center'
					data-aos='zoom-in'
					data-aos-delay='250'
				>
					{content.boxes.map((box) => (
						<div className='col-xl-2 col-md-4' key={box.text}>
							<div className='icon-box'>
								<i className={box.icon}></i>
								<h3>{box.text}</h3>
							</div>
						</div>
					))}
				</div>
			</div>
		</section>
	);
};

export default HomeSection;
