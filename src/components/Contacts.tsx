import React from 'react';

import content from '@/content/contacts.json';

const Contacts = (): React.ReactElement => {
	return (
		<section id='contacts' className='contacts'>
			<div className='container' data-aos='fade-up'>
				<div className='section-title'>
					<h2>{content.title}</h2>
					<p>{content.subtitle}</p>
				</div>

				<div className='info'>
					<div className='address'>
						<i className='bi bi-geo-alt'></i>
						<div>
							<h4>Адрес:</h4>
							<p>{content.address}</p>
						</div>
					</div>

					<div className='email'>
						<i className='bi bi-envelope'></i>
						<div>
							<h4>Email:</h4>
							<p>{content.email}</p>
						</div>
					</div>

					<div className='phone'>
						<i className='bi bi-phone'></i>
						<div>
							<h4>Телефон:</h4>
							<p>{content.phone}</p>
						</div>
					</div>
				</div>
			</div>
		</section>
	);
};

export default Contacts;
