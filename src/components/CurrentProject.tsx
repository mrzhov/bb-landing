import Link from 'next/link';
import React from 'react';

import content from '@/content/currentProject.json';

const CurrentProject = (): React.ReactElement => {
	return (
		<section id='project' className='features'>
			<div className='container' data-aos='fade-up'>
				<div className='row'>
					<div className='col-xl-6' data-aos='fade-right'>
						<div className='section-title'>
							<h2>{content.title}</h2>
							<p>{content.subtitle}</p>
							<div>{content.text}</div>
						</div>
					</div>
					<Link
						href={{
							pathname: '/project/[pageName]',
							query: { pageName: content.pageName },
						}}
					>
						<div className='col-xl-6 section-icon' data-aos='fade-left' data-aos-delay='100'>
							<div className='icon'>
								<i className={`bx ${content.project.icon}`}></i>
							</div>
							<h4>{content.project.title}</h4>
						</div>
					</Link>
				</div>
			</div>
		</section>
	);
};

export default CurrentProject;
