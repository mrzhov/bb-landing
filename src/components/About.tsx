import React from 'react';

import content from '@/content/about.json';

const About = (): React.ReactElement => {
	return (
		<section id='about' className='about'>
			<div className='container' data-aos='fade-up'>
				<div className='row'>
					<div
						className='col-lg-6 order-1 order-lg-2'
						data-aos='fade-left'
						data-aos-delay='100'
						style={{ position: 'relative' }}
					>
						<img className='img-fluid' src='/images/about.jpg' alt='Wallpaper' />
					</div>
					<div
						className='col-lg-6 pt-4 pt-lg-0 order-2 order-lg-1 content'
						data-aos='fade-right'
						data-aos-delay='100'
					>
						<h3>{content.title}</h3>
						<p className='fst-italic'>{content.descriptionTop}</p>
						<ul>
							{content.list.map((el) => (
								<li key={el}>
									<i className='ri-check-double-line'></i>
									<p>{el}</p>
								</li>
							))}
						</ul>
						<p>{content.descriptionBottom}</p>
					</div>
				</div>
			</div>
		</section>
	);
};

export default About;
