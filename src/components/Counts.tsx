import React from 'react';

import content from '@/content/counts.json';

const Counts = (): React.ReactElement => {
	return (
		<section id='counts' className='counts'>
			<div className='container' data-aos='fade-up'>
				<div className='row no-gutters'>
					<div
						className='image col-xl-5 d-flex align-items-stretch justify-content-center justify-content-lg-start'
						data-aos='fade-right'
						data-aos-delay='100'
					></div>
					<div
						className='col-xl-7 ps-0 ps-lg-5 pe-lg-1 d-flex align-items-stretch'
						data-aos='fade-left'
						data-aos-delay='100'
					>
						<div className='content d-flex flex-column justify-content-center'>
							<h3>{content.title}</h3>
							<p>{content.subtitle}</p>
							<div className='row'>
								{content.list.map((c) => (
									<div key={c.text} className='col-md-6 d-md-flex align-items-md-stretch'>
										<div className='count-box'>
											<i className={`bi ${c.icon}`}></i>
											<span>{c.endCounter}</span>
											<p>{c.text}</p>
										</div>
									</div>
								))}
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	);
};

export default Counts;
