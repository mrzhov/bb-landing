import Link from 'next/link';
import React from 'react';

import content from '@/content/navbar.json';

const Header = (): React.ReactElement => {
	return (
		<header id='header' className='fixed-top'>
			<div className='container d-flex align-items-center justify-content-lg-between'>
				<Link href='/'>
					<a className='logo me-auto me-lg-0'>
						<img className='img-fluid' src='/images/logo.png' alt='Wallpaper' />
					</a>
				</Link>

				<nav id='navbar' className='navbar order-last order-lg-0'>
					<ul id='navbar-ul'>
						{content.navbar.map((el) => (
							<li key={el.href}>
								<a className='nav-link scrollto' href={el.href}>
									{el.title}
								</a>
							</li>
						))}
					</ul>
					<i className='bi bi-list mobile-nav-toggle'></i>
				</nav>
			</div>
		</header>
	);
};

export default Header;
