import React from 'react';

import content from '@/content/features.json';

const Features = (): React.ReactElement => {
	return (
		<section id='features' className='features'>
			<div className='container' data-aos='fade-up'>
				<div className='row'>
					<div
						className='image col-lg-6'
						style={{ backgroundImage: 'url("/images/features.jpg")' }}
						data-aos='fade-right'
					></div>
					<div className='col-lg-6' data-aos='fade-left' data-aos-delay='100'>
						{content.features.map((el, index) => (
							<div
								key={el.title}
								className={`icon-box mt-5${!index ? ' mt-lg-0' : ''}`}
								data-aos='zoom-in'
								data-aos-delay='150'
							>
								<i className={`bx ${el.icon}`}></i>
								<h4>{el.title}</h4>
								<p>{el.subtitle}</p>
							</div>
						))}
					</div>
				</div>
			</div>
		</section>
	);
};

export default Features;
