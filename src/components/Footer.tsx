import React from 'react';

import content from '@/content/footer.json';

const Footer = (): React.ReactElement => {
	return (
		<footer id='footer'>
			<div className='container d-flex justify-content-center'>
				<div className='copyright'>
					&copy; Copyright{' '}
					<strong>
						<span>{content.companyName}</span>
					</strong>
					. All Rights Reserved
				</div>
			</div>
		</footer>
	);
};

export default Footer;
