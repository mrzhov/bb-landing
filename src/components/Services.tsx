import React from 'react';

import content from '@/content/services.json';

const Services = (): React.ReactElement => {
	return (
		<section id='services' className='services'>
			<div className='container' data-aos='fade-up'>
				<div className='section-title'>
					<h2>{content.title}</h2>
					<p>{content.subtitle}</p>
				</div>

				<div className='row'>
					{content.list.map((s, i) => (
						<div
							key={s.title}
							className={`col-lg-4 col-md-6 d-flex align-items-stretch${i ? ' mt-4' : ''}${
								i === 1 ? ' mt-md-0' : ''
							}${i === 2 ? ' mt-lg-0' : ''}`}
							data-aos='zoom-in'
							data-aos-delay='100'
						>
							<div className='icon-box'>
								<div className='icon'>
									<i className={`bx ${s.icon}`}></i>
								</div>
								<h4>{s.title}</h4>
								<p>{s.subtitle}</p>
							</div>
						</div>
					))}
				</div>
			</div>
		</section>
	);
};

export default Services;
