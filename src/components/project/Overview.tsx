import React from 'react';

// @ts-ignore
const Overview = ({ content }): React.ReactElement => {
	return (
		<section id='project-overview' className='d-flex align-items-center justify-content-center'>
			<div className='container' data-aos='fade-up'>
				<div className='row justify-content-center' data-aos='fade-up' data-aos-delay='150'>
					<div>
						<p id='project-overview-title'>{content.overview.title}</p>
						<br />
						<ol id='project-overview-subtitle'>
							{content.overview.subtitle.map((text: string, i: number) => (
								<li key={`subtitle-${i}`}>{text}</li>
							))}
						</ol>
					</div>
				</div>
			</div>
		</section>
	);
};
export default Overview;
