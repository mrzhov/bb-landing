import React from 'react';

// @ts-ignore
const Authors = ({ content }): React.ReactElement => {
	return (
		<section id='project-authors' className='d-flex align-items-left justify-content-left'>
			<div className='container' data-aos='fade-right'>
				<div
					style={{ width: 'fit-content' }}
					className='d-flex row justify-content-center'
					data-aos='fade-right'
					data-aos-delay='150'
				>
					<div style={{ flex: 1 }}>
						<p>Авторы:</p>
						{content.authors.map((author: string, i: number) => (
							<p key={`author-${i}`}>&ensp;{author}</p>
						))}
					</div>
					<div style={{ flex: 1 }}>
						<p>Эксперты:</p>
						{content.experts.map((expert: string, i: number) => (
							<p key={`expert-${i}`}>&ensp;{expert}</p>
						))}
					</div>
				</div>
			</div>
		</section>
	);
};
export default Authors;
