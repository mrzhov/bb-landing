import Link from 'next/link';
import React from 'react';

// @ts-ignore
const Content = ({ content }): React.ReactElement => {
	return (
		<section id='project-content' className='d-flex align-items-center justify-content-center'>
			<div className='container' data-aos='fade-up'>
				<div className='row justify-content-center' data-aos='fade-up' data-aos-delay='150'>
					<div>
						<p id='project-content-title'>{content.content.title}</p>
						<br />
						<ol id='project-content-elements'>
							{content.content.elements.map(
								(
									obj: {
										title: string;
										docs: { title: string; path: string; icon: string }[];
									},
									i: number,
								) => (
									<li key={`content-${i}`}>
										<p>{obj.title}</p>
										<div className='d-flex flex-wrap'>
											{obj.docs.map((element, j: number) => (
												<Link
													key={`content-${i}-element-${j}`}
													href={{
														pathname: '/document/[pageName]',
														query: { pageName: element.path },
													}}
												>
													<div
														style={{
															flexBasis: '31%',
															background: '#D9D9D9',
															padding: '24px',
														}}
														className='project-content-element d-flex flex-column'
													>
														<div
															style={{
																flex: 1,
																minWidth: '120px',
																maxWidth: '120px',
																minHeight: '120px',
																backgroundColor: '#FFC451',
															}}
														>
															<img
																src={`/images/projects/mracts/${element.icon}.svg`}
																height='80px'
																width='80px'
																style={{ margin: '20px' }}
															></img>
														</div>
														<br />
														<p style={{ flex: 1 }}>{element.title}</p>
													</div>
												</Link>
											))}
										</div>
									</li>
								),
							)}
						</ol>
					</div>
				</div>
			</div>
		</section>
	);
};
export default Content;
