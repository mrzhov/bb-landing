import React from 'react';

// @ts-ignore
const Title = ({ content }): React.ReactElement => {
	return (
		<section id='project-home' className='d-flex align-items-center justify-content-center'>
			<div className='container' data-aos='fade-up'>
				<div className='row justify-content-center' data-aos='fade-up' data-aos-delay='150'>
					<div>
						<h1 id='project-title'>{content.title}</h1>
						<br />
						<h2 id='project-year'>{content.year}</h2>
						<h2 id='project-subtitle'>{content.subtitle}</h2>
					</div>
				</div>
			</div>
		</section>
	);
};
export default Title;
