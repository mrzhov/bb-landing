import fs from 'fs';
import { join } from 'path';

const postsDirectory = join(process.cwd(), 'src/content/documents');

export function getPosts() {
	return fs.readdirSync(postsDirectory).map((path) => path.replace(/\.md$/, ''));
}

export function getPost(fileName: string) {
	const fullPath = join(postsDirectory, `${fileName}.md`);

	return fs.readFileSync(fullPath, 'utf8');
}
