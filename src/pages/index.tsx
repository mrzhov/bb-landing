import type { NextPage } from 'next';

import About from '@/components/About';
import Contacts from '@/components/Contacts';
import Counts from '@/components/Counts';
import CurrentProject from '@/components/CurrentProject';
import Features from '@/components/Features';
import Footer from '@/components/Footer';
import Header from '@/components/Header';
import HomeSection from '@/components/HomeSection';
import Services from '@/components/Services';

const Home: NextPage = () => {
	return (
		<div>
			<Header />
			<HomeSection />
			<main id='main'>
				<About />
				<Features />
				<Counts />
				<Services />
				<CurrentProject />
				<Contacts />
				<Footer />
			</main>
			<div id='preloader'></div>
			<a href='#' className='back-to-top d-flex align-items-center justify-content-center'>
				<i className='bi bi-arrow-up-short'></i>
			</a>
		</div>
	);
};

export default Home;
