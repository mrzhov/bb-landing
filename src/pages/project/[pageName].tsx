import type { NextPage } from 'next';

import Header from '@/components/Header';
import Authors from '@/components/project/Authors';
import Content from '@/components/project/Content';
import Overview from '@/components/project/Overview';
import Title from '@/components/project/Title';

// @ts-ignore
const Project: NextPage = ({ content }) => {
	return (
		<div>
			<Header />
			<Title content={content} />
			<main id='project-main'>
				<Authors content={content} />
				<Overview content={content} />
				<Content content={content} />
			</main>
			<div id='preloader'></div>
			<a href='#' className='back-to-top d-flex align-items-center justify-content-center'>
				<i className='bi bi-arrow-up-short'></i>
			</a>
		</div>
	);
};

export async function getStaticPaths() {
	const projects = await import('@/content/projects');

	// @ts-ignore
	const paths = Object.keys(projects).map((pageName) => ({
		params: { pageName },
	}));
	return { paths, fallback: false };
}

// @ts-ignore
export async function getStaticProps({ params }) {
	const projects = await import('@/content/projects');
	// @ts-ignore
	return { props: { content: projects[params.pageName] } };
}

export default Project;
