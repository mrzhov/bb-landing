import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { TreeView } from '@mui/lab';
import TreeItem from '@mui/lab/TreeItem';
import type { NextPage } from 'next';
import React from 'react';
import ReactMarkdown from 'react-markdown';
import rehypeFormat from 'rehype-format';
import rehypeSanitize from 'rehype-sanitize';
import rehypeSlug from 'rehype-slug';
import rehypeStringify from 'rehype-stringify';
import remarkGfm from 'remark-gfm';
import remarkParse from 'remark-parse';
import remarkRehype from 'remark-rehype';
import remarkToc from 'remark-toc';
import { unified } from 'unified';

import Header from '@/components/Header';
import { getPost, getPosts } from '@/utils/lib';

const { html2json } = require('html2json');

// @ts-ignore
const Document: NextPage = ({ content, toc }) => {
	let i = 1;
	const goToElement = (event: React.MouseEvent<HTMLElement>, key: string) => {
		event.preventDefault();
		const anchor = document.getElementById(decodeURI(key));
		if (anchor) {
			const y = anchor.getBoundingClientRect().top + window.pageYOffset - 100;
			window.scrollTo({ top: y, behavior: 'smooth' });
		}
	};

	const getOnClick = (value: any) => (event: React.MouseEvent<HTMLElement>) => {
		if (event.target instanceof HTMLDivElement) return goToElement(event, value);
		return {};
	};

	const handleUL = (ul: any) => {
		if (!ul.child || ul.child.length === 0) return [];
		const rawChildren = ul.child.filter((child: any) => child.tag === 'li');
		return rawChildren.map((rawChild: any) => handleLI(rawChild));
	};

	const handleLI = (li: any) => {
		let text = 'Не найдено';
		let href = 'Не найдено';
		const Pchildren = li.child.filter((child: any) => child.tag === 'p');
		const Achildren = li.child.filter((child: any) => child.tag === 'a');
		if (Pchildren.length === 1) {
			text = Pchildren[0].child.filter((child: any) => child.tag === 'a')[0].child[0].text;
			href = Pchildren[0].child.filter((child: any) => child.tag === 'a')[0].attr.href;
		} else if (Achildren.length === 1) {
			text = Achildren[0].child[0].text;
			href = Achildren[0].attr.href;
		}
		const children = li.child.filter((child: any) => child.tag === 'ul');
		const nodeId = i.toString();
		i += 1;
		return (
			<TreeItem
				ContentProps={{ style: { marginTop: 1, padding: 1 } }}
				onClick={getOnClick(href.replace('#', ''))}
				nodeId={nodeId}
				label={text}
				key={`treeItem-${nodeId}`}
			>
				{children && children.map((childUl: any) => handleUL(childUl))}
			</TreeItem>
		);
	};

	return (
		<div>
			<Header />
			<div
				style={{
					display: 'flex',
					flexDirection: 'row',
					marginTop: '100px',
					padding: '24px',
					height: 'calc(100vh - 100px)',
				}}
				id='document-main'
			>
				<div id='toc'>
					<TreeView
						sx={{
							width: 400,
							flexGrow: 1,
							height: 'auto',
						}}
						defaultCollapseIcon={<ExpandMoreIcon />}
						defaultExpandIcon={<ChevronRightIcon />}
					>
						{handleUL(html2json(toc).child[0])}
					</TreeView>
				</div>
				<div style={{ flex: 1, marginLeft: '450px', marginBottom: '100px' }}>
					<ReactMarkdown
						remarkPlugins={[remarkGfm]}
						rehypePlugins={[rehypeFormat, rehypeSanitize, rehypeSlug]}
					>
						{content}
					</ReactMarkdown>
					<br />
				</div>
			</div>
			<div id='preloader'></div>
			<a href='#' className='back-to-top d-flex align-items-center justify-content-center'>
				<i className='bi bi-arrow-up-short'></i>
			</a>
		</div>
	);
};

async function createTOC(content: any) {
	const result = (
		await unified()
			.use(remarkParse)
			.use(remarkGfm)
			.use(remarkToc)
			.use(remarkRehype)
			.use(rehypeSlug)
			.use(rehypeStringify)
			.process(`# toc\n ${content}`)
	).toString();
	return result
		.substring(result.indexOf('toc</h1>') + 8, result.indexOf('</ul>\n<h1 id=') + 5)
		.trim();
}

export async function getStaticPaths() {
	const documents = getPosts();
	const paths = documents.map((pageName) => ({
		params: { pageName },
	}));
	return { paths, fallback: false };
}

// @ts-ignore
export async function getStaticProps({ params }) {
	const content = getPost(params.pageName);
	const toc = await createTOC(content);
	return {
		props: { content, toc },
	};
}

export default Document;
