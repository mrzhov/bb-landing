import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-icons/font/bootstrap-icons.css';
import 'remixicon/fonts/remixicon.css';
import 'aos/dist/aos.css';
import 'swiper/css';
import '@/styles/globals.css';

import type { AppProps } from 'next/app';
import Head from 'next/head';
import { useEffect } from 'react';

import { main } from '@/utils/main';

function MyApp({ Component, pageProps }: AppProps) {
	if (process.browser) {
		useEffect(() => {
			main();
		}, []);
	}

	return (
		<div>
			<Head>
				<meta name='viewport' content='width=device-width, initial-scale=1' />
				<title>Цифровая экспертиза</title>
			</Head>
			<Component {...pageProps} />
		</div>
	);
}

export default MyApp;
